let f0 = Number(prompt('Enter F0'));
let f1 = Number(prompt('Enter F1'));
let n = Number(prompt('Enter n'));
let result = 0;
function fib() {
if(n == 0){
	return f0;
}
else if(n == 1){
	return f1;
}
else if(n >= 2){
	for (let i = 2; i <= n; i++) {
	    result = f0 + f1;
	    f0 = f1;
	    f1 = result;
  	}
 	return result;
}
else if(n < 0){
	for (let i = -1; i >= n; i--) {
	    result = f1 - f0;
	    f1 = f0;
	    f0 = result;
  	}
	return result;
}
}
console.log(fib())