function circleCreate(){
	$('#button-1').bind('click', function(){
		$('#button-1').css({display:'none'})
		$('body').append('<input placeholder="Введите диаметр" id="inp1">')
		$('body').append('<button id="button-2">Нарисовать</button>')
		$('#button-2').bind('click', function(){
			let diametr = $('#inp1').val()
			$('#button-2').css({display:'none'})
			$('#inp1').css({display:'none'})
			$('body').append('<div id="main"></div>')
			$('#main').css({width: diametr*10 ,height: diametr*10,display:'flex',flexWrap:'wrap'})
			function randomColor(){
				let color = 'ABCDE0123456789'.split('')
				let s = '#'
				for (let i = 0; i < 6; i++ ) {
	       				s += color[Math.round(Math.random() * 14)];
	    		}
				return s;
			}
			for(let i = 0;i<100;i++){
				$('#main').append('<div class="divs"></div>')
				$('.divs').css({width: diametr ,height: diametr,borderRadius: diametr/2,background:randomColor})
				$('.divs').bind('click',function(e){
					$(e.target).css('visibility','hidden')
				})
			}
		})
	})
}
circleCreate()